import './App.css';
import Button from './components/Button';
import Modal from './components/Modal';
import { useState } from 'react';


function App() {
  const [isModalOpen, setIsModalOpen] = useState(false)
  const [isSecondModalOpen, setIsSecondModalOpen] = useState(false)

  const openModal = () => {
    setIsModalOpen(true);
  }

  const openSecondModal = () => {
    setIsSecondModalOpen(true);
  }

  const closeModal = () => {
    setIsModalOpen(false);
  }

  const closeSecondModal = () => {
    setIsSecondModalOpen(false);
  }

  const modalActions = (
    <>
      <button className='modal-buttons__item' onClick={closeModal}>Ok</button>
      <button className='modal-buttons__item' onClick={closeModal}>Cancel</button>
    </>
  )

  return (
    <div className="App">
      <Button text="Open first modal" backgroundColor="#66849a" onClick={openModal} />
      <Button text="Open second modal" backgroundColor="#d4ba6a" onClick={openSecondModal} />
      <Modal 
        isOpen={isModalOpen} 
        onClose={closeModal} 
        header="Do you want to delete this file ?" 
        text="Once you delete this file, it won't be possible to undo this action. Are you shure you want to delete it ?"
        actions={modalActions}
      />
      <Modal 
        isOpen={isSecondModalOpen} 
        onClose={closeSecondModal} 
        header="Do you like it ?" 
        text="Please, tell me if you like how I completed this task."
        actions={modalActions}
      />
    </div>
  );
}

export default App;
